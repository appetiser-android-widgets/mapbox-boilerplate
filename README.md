# mapbox-boilerplate

Android boilerplate for Mapbox.

## Getting started

### Access Token

You need to obtain an access token from [Mapbox](https://docs.mapbox.com/help/how-mapbox-works/access-tokens/).

#### Placing access token in project

1. After obtaining the access token, in your Android Studio. Create a `secret.gradle` file at the root level of your project and place the access token inside like this:
```
ext {
    mapbox = [
          access_token: '<your_mapbox_access_token>'
    ]
}
```

2. Open your app level `build.gradle` file and add these lines
```
apply from: '../secret.gradle'
...
...
...

android {
   ...
   ...
   
   buidTypes {
       debug {
           resValue "string", "mapbox_access_token", mapbox.access_token
       }
   }
}
```

3. Initialize mapbox in your Application class
```
class YourApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        Mapbox
            .getInstance(
                applicationContext,
                getString(R.string.mapbox_access_token)
            )
    }
}
```

And you're all set!

# 
## How to add in your project

1. Clone the project in your local using this
```
git clone git@gitlab.com:appetiser-android-widgets/mapbox-boilerplate.git
```

2. In your existing project where you will import the mapbox boilerplate. Switch to **Project** view.
3. Right click your project's root folder and select **"New"** -> **"Module"**.
4. In **"Create New Module"** dialog, select **"Import Gradle Project"**.
5. Navigate to the cloned project and select **"android-mapbox"** -> **"androidmapbox"**.
6. In your app's `build.gradle`, add dependency to the androidmapbox boilerplate using this:
```
implementation project(":androidmapbox")
```

You should now be able to use it!

#
## How to run demo project
1. Clone the project in your local using this
```
git clone git@gitlab.com:appetiser-android-widgets/mapbox-boilerplate.git
```
2. Run the app `androidmapboxdemo`

Happy testing!
