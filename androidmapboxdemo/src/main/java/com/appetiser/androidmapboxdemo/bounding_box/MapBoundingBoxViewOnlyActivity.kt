package com.appetiser.androidmapboxdemo.bounding_box

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.models.BoundingBox
import com.appetiser.androidmapbox.models.Coordinates
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapBoundingBoxViewOnlyActivity : BaseMapActivity() {
    companion object {
        const val EXTRA_TOP_LEFT_LATITUDE = "EXTRA_TOP_LEFT_LATITUDE"
        const val EXTRA_TOP_LEFT_LONGITUDE = "EXTRA_TOP_LEFT_LONGITUDE"
        const val EXTRA_BOTTOM_RIGHT_LATITUDE = "EXTRA_BOTTOM_RIGHT_LATITUDE"
        const val EXTRA_BOTTOM_RIGHT_LONGITUDE = "EXTRA_BOTTOM_RIGHT_LONGITUDE"

        fun open(context: Context, extras: Bundle) {
            Intent(context, MapBoundingBoxViewOnlyActivity::class.java)
                .apply {
                    putExtras(extras)
                    context.startActivity(this)
                }
        }
    }

    private lateinit var boundingBox: BoundingBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_bounding_box_view_only)

        val topLeftLatitude =
            intent
                .extras
                ?.getDouble(EXTRA_TOP_LEFT_LATITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_TOP_LEFT_LATITUDE")

        val topLeftLongitude =
            intent
                .extras
                ?.getDouble(EXTRA_TOP_LEFT_LONGITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_TOP_LEFT_LONGITUDE")

        val bottomRightLatitude =
            intent
                .extras
                ?.getDouble(EXTRA_BOTTOM_RIGHT_LATITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_BOTTOM_RIGHT_LATITUDE")

        val bottomRightLongitude =
            intent
                .extras
                ?.getDouble(EXTRA_BOTTOM_RIGHT_LONGITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_BOTTOM_RIGHT_LONGITUDE")

        boundingBox = BoundingBox(
            Coordinates(
                topLeftLatitude,
                topLeftLongitude
            ),
            Coordinates(
                bottomRightLatitude,
                bottomRightLongitude
            )
        )
    }

    override fun getMapCenterView(): View? = null

    override fun isViewOnly(): Boolean = true

    override fun onStyleLoaded() {
        drawCircleAtBoundBox(
            boundingBox.topLeft.latitude,
            boundingBox.topLeft.longitude,
            boundingBox.bottomRight.latitude,
            boundingBox.bottomRight.longitude
        )
    }

    override fun onMapReady() {
        changeMapCameraPositionByBoundingBox(
            boundingBox.topLeft.latitude,
            boundingBox.topLeft.longitude,
            boundingBox.bottomRight.latitude,
            boundingBox.bottomRight.longitude
        )
    }

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}