package com.appetiser.androidmapboxdemo.bounding_box

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapBoundingBoxActivity : BaseMapActivity() {
    companion object {
        fun open(context: Context) {
            context
                .startActivity(
                    Intent(context, MapBoundingBoxActivity::class.java)
                )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_bounding_box)
    }

    override fun onMapReady() {
        findViewById<Button>(R.id.button)
            .setOnClickListener {
                navigateToViewOnlyActivity()
            }
    }

    private fun navigateToViewOnlyActivity() {
        val currentMapCenterBoundingBox = getMapCenterBoundingBox()!!
        val extras = Bundle()
            .apply {
                putDouble(
                    MapBoundingBoxViewOnlyActivity.EXTRA_TOP_LEFT_LATITUDE,
                    currentMapCenterBoundingBox.topLeft.latitude
                )

                putDouble(
                    MapBoundingBoxViewOnlyActivity.EXTRA_TOP_LEFT_LONGITUDE,
                    currentMapCenterBoundingBox.topLeft.longitude
                )

                putDouble(
                    MapBoundingBoxViewOnlyActivity.EXTRA_BOTTOM_RIGHT_LATITUDE,
                    currentMapCenterBoundingBox.bottomRight.latitude
                )

                putDouble(
                    MapBoundingBoxViewOnlyActivity.EXTRA_BOTTOM_RIGHT_LONGITUDE,
                    currentMapCenterBoundingBox.bottomRight.longitude
                )
            }

        MapBoundingBoxViewOnlyActivity.open(
            this,
            extras
        )
    }

    override fun onStyleLoaded() = Unit

    override fun isViewOnly(): Boolean = false

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapCenterView(): View = findViewById(R.id.mapCenter)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}