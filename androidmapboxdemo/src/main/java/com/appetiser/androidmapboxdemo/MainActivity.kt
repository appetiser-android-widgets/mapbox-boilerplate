package com.appetiser.androidmapboxdemo

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.androidmapboxdemo.bounding_box.MapBoundingBoxActivity
import com.appetiser.androidmapboxdemo.current_location.MapCurrentLocationPinActivity
import com.appetiser.androidmapboxdemo.labeled_location.MapLabeledLocationActivity
import com.appetiser.androidmapboxdemo.single_location_pin.MapSingleLocationPinActivity
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager

class MainActivity : AppCompatActivity(), PermissionsListener {

    private val permissionsManager: PermissionsManager by lazy {
        PermissionsManager(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btnBoundingBox)
            .setOnClickListener {
                MapBoundingBoxActivity.open(this)
            }

        findViewById<Button>(R.id.btnSingleLocationPin)
            .setOnClickListener {
                MapSingleLocationPinActivity.open(this)
            }

        findViewById<Button>(R.id.btnShowCurrentLocation)
            .setOnClickListener {
                permissionsManager.requestLocationPermissions(this)
            }

        findViewById<Button>(R.id.btnLabeledLocation)
            .setOnClickListener {
                MapLabeledLocationActivity.open(this)
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            MapCurrentLocationPinActivity.open(this)
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) = Unit
}