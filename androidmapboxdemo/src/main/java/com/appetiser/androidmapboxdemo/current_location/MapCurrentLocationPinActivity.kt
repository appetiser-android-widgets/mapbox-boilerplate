package com.appetiser.androidmapboxdemo.current_location

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapCurrentLocationPinActivity : BaseMapActivity() {

    companion object {
        fun open(context: Context) {
            context
                .startActivity(
                    Intent(context, MapCurrentLocationPinActivity::class.java)
                )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_current_location)
    }

    override fun onStyleLoaded() {
        showUserLocationMarker()
    }

    override fun onMapReady() = Unit

    override fun getMapCenterView(): View? = null

    override fun isViewOnly(): Boolean = true

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}