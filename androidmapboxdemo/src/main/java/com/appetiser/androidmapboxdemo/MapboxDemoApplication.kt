package com.appetiser.androidmapboxdemo

import android.app.Application
import com.mapbox.mapboxsdk.Mapbox

class MapboxDemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        // Mapbox Access token
        Mapbox
            .getInstance(
                applicationContext,
                getString(R.string.mapbox_access_token)
            )
    }
}