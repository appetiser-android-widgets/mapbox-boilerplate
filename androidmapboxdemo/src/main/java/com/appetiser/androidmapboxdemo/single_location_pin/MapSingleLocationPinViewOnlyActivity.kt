package com.appetiser.androidmapboxdemo.single_location_pin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.models.Coordinates
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapSingleLocationPinViewOnlyActivity : BaseMapActivity() {
    companion object {
        const val EXTRA_LATITUDE = "EXTRA_LATITUDE"
        const val EXTRA_LONGITUDE = "EXTRA_LONGITUDE"

        fun open(context: Context, extras: Bundle) {
            Intent(context, MapSingleLocationPinViewOnlyActivity::class.java)
                .apply {
                    putExtras(extras)
                    context.startActivity(this)
                }
        }
    }

    private lateinit var coordinates: Coordinates

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_single_location_pin_view_only)

        val latitude =
            intent
                .extras
                ?.getDouble(EXTRA_LATITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_LATITUDE")

        val longitude =
            intent
                .extras
                ?.getDouble(EXTRA_LONGITUDE)
                ?: throw IllegalArgumentException("Missing required extra $EXTRA_LONGITUDE")

        coordinates = Coordinates(latitude, longitude)
    }

    override fun onMapReady() {
        changeMapCameraPositionByCoordinates(coordinates)
    }

    override fun onStyleLoaded() {
        drawMarkerAtCoordinates(
            coordinates,
            ContextCompat.getDrawable(this, R.drawable.ic_marker)!!
        )
    }

    override fun getMapCenterView(): View? = null

    override fun isViewOnly(): Boolean = true

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}