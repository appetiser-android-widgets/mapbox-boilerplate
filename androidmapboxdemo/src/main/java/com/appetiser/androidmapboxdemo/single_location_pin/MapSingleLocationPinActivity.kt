package com.appetiser.androidmapboxdemo.single_location_pin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapSingleLocationPinActivity : BaseMapActivity() {
    companion object {
        fun open(context: Context) {
            context
                .startActivity(
                    Intent(context, MapSingleLocationPinActivity::class.java)
                )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_single_location_pin)
    }

    override fun onMapReady() {
        findViewById<Button>(R.id.button)
            .setOnClickListener {
                navigateToViewOnlyActivity()
            }
    }

    private fun navigateToViewOnlyActivity() {
        val mapCenterCoordinates = getMapCenterCoordinates()
        val extras = Bundle()
            .apply {
                putDouble(
                    MapSingleLocationPinViewOnlyActivity.EXTRA_LATITUDE,
                    mapCenterCoordinates.latitude
                )
                putDouble(
                    MapSingleLocationPinViewOnlyActivity.EXTRA_LONGITUDE,
                    mapCenterCoordinates.longitude
                )
            }

        MapSingleLocationPinViewOnlyActivity
            .open(
                this,
                extras
            )
    }

    override fun onStyleLoaded() = Unit

    override fun isViewOnly(): Boolean = false

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapCenterView(): View = findViewById(R.id.mapCenter)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}