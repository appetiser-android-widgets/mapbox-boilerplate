package com.appetiser.androidmapboxdemo.labeled_location

import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.appetiser.androidmapbox.BaseMapActivity
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapboxdemo.R
import com.mapbox.mapboxsdk.maps.MapView

class MapLabeledLocationActivity : BaseMapActivity() {

    private val geocoder: Geocoder by lazy {
        Geocoder(this)
    }

    companion object {
        fun open(context: Context) {
            context
                .startActivity(
                    Intent(context, MapLabeledLocationActivity::class.java)
                )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_labeled_location)
    }

    override fun onMapReady() {
        findViewById<Button>(R.id.buttonAddCustomView)
            .setOnClickListener {
                getLocalityAndAddCustomView()
            }
        findViewById<Button>(R.id.buttonLabeledLocationPin)
            .setOnClickListener {
                getLocalityAndAddLabeledPin()
            }
    }

    private fun getLocalityAndAddCustomView() {
        createCustomLocalityView()?.let {
            drawCustomView(it, getMapCenterCoordinates())
        }
    }

    private fun createCustomLocalityView(): View? {
        val view = layoutInflater.inflate(R.layout.view_marker, null)
        view.findViewById<AppCompatTextView>(R.id.textLabel)
            .text = getLocality()
        return view
    }

    private fun getLocalityAndAddLabeledPin() {
        val drawable =
            ResourcesCompat
                .getDrawable(
                    resources,
                    R.drawable.ic_marker_blue,
                    theme
                )

        drawable?.let {
            drawLabeledMarker(it, getMapCenterCoordinates(), getLocality())
        }
    }

    private fun getLocality(): String {
        val mapCenterCoordinates = getMapCenterCoordinates()
        val address = geocoder
            .getFromLocation(
                mapCenterCoordinates.latitude,
                mapCenterCoordinates.longitude,
                1
            )
        return when {
            address.isNullOrEmpty() || address.first().locality.isNullOrBlank() -> {
                getString(R.string.unnamed_locality)
            }
            else -> {
                address.first().locality
            }
        }
    }

    override fun onStyleLoaded() = Unit

    override fun isViewOnly(): Boolean = false

    override fun getMapView(): MapView = findViewById(R.id.mapView)

    override fun getMapCenterView(): View = findViewById(R.id.mapCenter)

    override fun getMapMaxZoom(): Double = DEFAULT_MAP_ZOOM

    override fun getMapboxAccessToken(): String = getString(R.string.mapbox_access_token)
}