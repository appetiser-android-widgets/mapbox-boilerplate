package com.appetiser.androidmapbox

import android.graphics.Color
import android.graphics.PointF
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.appetiser.androidmapbox.models.BoundingBox
import com.appetiser.androidmapbox.models.Coordinates
import com.appetiser.androidmapbox.utils.DEFAULT_MAP_ZOOM
import com.appetiser.androidmapbox.utils.SymbolGenerator
import com.appetiser.androidmapbox.utils.calculateRadius
import com.appetiser.androidmapbox.utils.toPx
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.FillManager
import com.mapbox.mapboxsdk.plugins.annotation.FillOptions
import com.mapbox.mapboxsdk.plugins.annotation.LineManager
import com.mapbox.mapboxsdk.plugins.annotation.LineOptions
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.turf.TurfTransformation

abstract class BaseMapFragment : Fragment() {

    companion object {
        private const val TAG = "BaseMapFragment"
        const val MARKER_ICON_ID = "MARKER_ICON_ID"
        const val MARKER_SYMBOL_LAYER_ID = "MARKER_SYMBOL_LAYER_ID"
        const val MARKER_GEO_SOURCE_ID = "MARKER_GEO_SOURCE_ID"
        const val LABELED_MARKER_ICON_ID = "LABELED_MARKER_ICON_ID"
        const val LABELED_MARKER_SYMBOL_LAYER_ID = "LABELED_MARKER_SYMBOL_LAYER_ID"
        const val LABELED_MARKER_GEO_SOURCE_ID = "LABELED_MARKER_GEO_SOURCE_ID"
        const val CUSTOM_MARKER_ICON_ID = "CUSTOM_MARKER_ICON_ID"
        const val CUSTOM_MARKER_SYMBOL_LAYER_ID = "CUSTOM_MARKER_SYMBOL_LAYER_ID"
        const val CUSTOM_MARKER_GEO_SOURCE_ID = "CUSTOM_MARKER_GEO_SOURCE_ID"
    }

    private val fillOptions by lazy {
        FillOptions()
            .withFillColor(
                PropertyFactory
                    .circleStrokeColor(
                        ContextCompat.getColor(
                            requireContext(),
                            android.R.color.transparent
                        )
                    )
                    .value
            )
    }

    private val lineOptions by lazy {
        LineOptions()
            .withLineColor(
                PropertyFactory
                    .circleStrokeColor(
                        ContextCompat.getColor(
                            requireContext(),
                            android.R.color.holo_red_dark
                        )
                    )
                    .value
            )
            .withLineWidth(1.toPx(requireContext()).toFloat())
    }

    private val locationComponentOptions by lazy {
        LocationComponentOptions
            .builder(requireContext())
            .foregroundTintColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.holo_red_dark
                )
            )
            .backgroundTintColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.holo_red_dark
                )
            )
            .bearingTintColor(null)
            .accuracyAlpha(0f)
            .build()
    }

    var mapBoxMap: MapboxMap? = null

    /**
     * Top left point of the map center view from [getMapCenterView].
     * We will be using this in getting the bounding box of the center of map view inside [getMapCenterView].
     *
     * NOTE: The x and y coordinates here are relative to the mapview and not the entire screen.
     */
    private lateinit var mapCenterTopLeftPoint: PointF

    /**
     * Bottom right point of the map center view from [getMapCenterView].
     * We will be using this in getting the bounding box of the center of map view inside [getMapCenterView].
     *
     * NOTE: The x and y coordinates here are relative to the mapview and not the entire screen.
     */
    private lateinit var mapCenterBottomRightPoint: PointF

    /**
     * Callback when map is ready to use.
     */
    abstract fun onMapReady()

    /**
     * Return MapView from fragment's view.
     */
    abstract fun getMapView(): MapView

    /**
     * Return MapView center view indicator. Return null if [isViewOnly] is true.
     */
    abstract fun getMapCenterView(): View?

    /**
     * Return MapView's max zoom.
     */
    abstract fun getMapMaxZoom(): Double

    /**
     * Return Mapbox access token.
     *
     * Please get it here: [https://docs.mapbox.com/help/how-mapbox-works/access-tokens/].
     */
    abstract fun getMapboxAccessToken(): String

    /**
     * Callback when map style has already loaded.
     */
    abstract fun onStyleLoaded()

    /**
     * Override this method to disable all UI gestures to map.
     */
    open fun enableAllGestures(): Boolean = true

    /**
     * Return true if map does not accept any user action or inputs (i.e. zoom, pan, etc.).
     */
    abstract fun isViewOnly(): Boolean

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Mapbox
            .getInstance(
                requireContext(),
                getMapboxAccessToken()
            )
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getMapView()
            .onCreate(savedInstanceState)
    }

    private fun initializeView() {
        getMapCenterScreenCoordinates()
    }

    /**
     * Re-calculates map center coordinates.
     * Use this just in case you move [getMapCenterView] or scale its size. Like zooming in.
     */
    fun recalculateMapCenterCoordinates() {
        calculateMapCenterCoordinates()
    }

    private fun getMapCenterScreenCoordinates() {
        getMapView()
            .viewTreeObserver
            .addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    getMapView()
                        .viewTreeObserver
                        .removeOnGlobalLayoutListener(this)

                    if (!isViewOnly()) {
                        calculateMapCenterCoordinates()
                    }
                }
            })
    }

    /**
     * Calculates map center coordinates.
     * We will be using this in getting the bounding box of the map view.
     */
    private fun calculateMapCenterCoordinates() {
        val centerView = getMapCenterView() ?: return

        val mapCenterTopLeft = IntArray(2)
        centerView.getLocationOnScreen(mapCenterTopLeft)

        val mapTopLeft = IntArray(2)
        getMapView().getLocationOnScreen(mapTopLeft)

        val mapCenterBottomRightX = mapCenterTopLeft[0] + centerView.width
        val mapCenterBottomRightY = mapCenterTopLeft[1] + centerView.height

        mapCenterTopLeftPoint = PointF(
            (mapCenterTopLeft[0] - mapTopLeft[0]).toFloat(),
            (mapCenterTopLeft[1] - mapTopLeft[1]).toFloat()
        )

        mapCenterBottomRightPoint = PointF(
            (mapCenterBottomRightX - mapTopLeft[0]).toFloat(),
            (mapCenterBottomRightY - mapTopLeft[1]).toFloat()
        )

        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterWidth: ${centerView.width}")
        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterHeight: ${centerView.height}")

        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterTopLeftX: ${mapCenterTopLeft[0]}")
        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterTopLeftY: ${mapCenterTopLeft[1]}")

        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterBottomRightX: $mapCenterBottomRightX")
        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterBottomRightY: $mapCenterBottomRightY")

        Log.d(TAG, "getMapCenterScreenCoordinates mapTopLeftX: ${mapTopLeft[0]}")
        Log.d(TAG, "getMapCenterScreenCoordinates mapTopLeftY: ${mapTopLeft[1]}")

        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterTopLeftX relative: ${mapCenterTopLeftPoint.x}")
        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterTopLeftY relative: ${mapCenterTopLeftPoint.y}")

        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterBottomRightX relative: ${mapCenterBottomRightPoint.x}")
        Log.d(TAG, "getMapCenterScreenCoordinates mapCenterBottomRightY: ${mapCenterBottomRightPoint.y}")
    }

    private fun setupMapView() {
        getMapView()
            .getMapAsync { map ->
                mapBoxMap = map
                mapBoxMap!!.setStyle(Style.MAPBOX_STREETS) {
                    val uiSettings = map.uiSettings
                    uiSettings.setAllGesturesEnabled(enableAllGestures())

                    onStyleLoaded()
                }
                mapBoxMap!!.setMaxZoomPreference(getMapMaxZoom())

                onMapReady()
            }
    }

    /**
     * Zooms the current center of the map by [zoom] level.
     *
     * @param zoom Zoom level. Must not be greater than [getMapMaxZoom]
     */
    fun changeMapZoom(zoom: Double) {
        mapBoxMap
            ?.let { map ->
                val currentMapCenter = map.cameraPosition.target
                changeMapCameraPositionByCoordinates(
                    Coordinates(
                        currentMapCenter.latitude,
                        currentMapCenter.longitude
                    ),
                    zoom
                )
            }
    }

    /**
     * Updates map view position position by [Coordinates].
     *
     * @param coordinates new map position
     * @param zoom Zoom level. Must not be greater than [getMapMaxZoom]
     */
    fun changeMapCameraPositionByCoordinates(
        coordinates: Coordinates,
        zoom: Double = DEFAULT_MAP_ZOOM
    ) {
        CameraPosition.Builder()
            .target(LatLng(coordinates.latitude, coordinates.longitude))
            .zoom(zoom)
            .build()
            .apply {
                mapBoxMap
                    ?.animateCamera(
                        CameraUpdateFactory
                            .newCameraPosition(
                                this
                            ),
                        500
                    )
            }
    }

    /**
     * Updates map view position position by [LatLngBounds].
     */
    fun changeMapCameraPositionByLatLngBounds(
        topLeftLatitude: Double,
        topLeftLongitude: Double,
        bottomRightLatitude: Double,
        bottomRightLongitude: Double
    ) {
        val position =
            LatLngBounds.Builder()
                .include(LatLng(topLeftLatitude, topLeftLongitude))
                .include(LatLng(bottomRightLatitude, bottomRightLongitude))
                .build()

        mapBoxMap!!.animateCamera(
            CameraUpdateFactory
                .newLatLngBounds(
                    position,
                    0
                )
        )
    }

    /**
     * Returns bounding box of map center view from [getMapCenterView].
     */
    fun getMapCenterBoundingBox(): BoundingBox? {
        if (mapBoxMap != null) {
            val selectedLocationTopLeft = mapBoxMap!!
                .projection
                .fromScreenLocation(
                    mapCenterTopLeftPoint
                )

            val selectedLocationBottomRight = mapBoxMap!!
                .projection
                .fromScreenLocation(
                    mapCenterBottomRightPoint
                )

            return BoundingBox(
                Coordinates(
                    selectedLocationTopLeft.latitude,
                    selectedLocationTopLeft.longitude
                ),
                Coordinates(
                    selectedLocationBottomRight.latitude,
                    selectedLocationBottomRight.longitude
                )
            )
        }

        // mapbox is null
        return null
    }

    /**
     * Returns center of map [Coordinates].
     */
    fun getMapCenterCoordinates(): Coordinates {
        val center = mapBoxMap!!.cameraPosition.target

        return Coordinates(
            center.latitude,
            center.longitude
        )
    }

    /**
     * Draws circle at the specified bounding box.
     *
     * NOTE: Make sure to call this after [onStyleLoaded].
     */
    fun drawCircleAtBoundBox(
        topLeftLatitude: Double,
        topLeftLongitude: Double,
        bottomRightLatitude: Double,
        bottomRightLongitude: Double
    ) {
        val boundingBoxCenterCoordinates =
            BoundingBox
                .getBoundingBoxCenterCoordinates(
                    topLeftLatitude,
                    topLeftLongitude,
                    bottomRightLatitude,
                    bottomRightLongitude
                )

        val radiusInKm =
            calculateRadius(
                topLeftLatitude,
                boundingBoxCenterCoordinates
            )

        val turf =
            TurfTransformation
                .circle(
                    Point.fromLngLat(
                        boundingBoxCenterCoordinates.longitude,
                        boundingBoxCenterCoordinates.latitude
                    ),
                    radiusInKm,
                    "kilometers"
                )

        FillManager(getMapView(), mapBoxMap!!, mapBoxMap!!.style!!)
            .apply {
                create(
                    fillOptions.withGeometry(turf)
                )
            }

        LineManager(getMapView(), mapBoxMap!!, mapBoxMap!!.style!!)
            .apply {
                create(
                    lineOptions.withGeometry(turf.outer())
                )
            }
    }

    /**
     * Draws marker at specified [Coordinates].
     */
    fun drawMarkerAtCoordinates(coordinates: Coordinates, markerDrawable: Drawable) {
        val style = mapBoxMap!!.style!!
        val features =
            listOf(
                Feature.fromGeometry(
                    Point.fromLngLat(
                        coordinates.longitude,
                        coordinates.latitude
                    )
                )
            )

        style
            .addImage(
                MARKER_ICON_ID,
                markerDrawable
            )

        style
            .addSource(
                GeoJsonSource(
                    MARKER_GEO_SOURCE_ID,
                    FeatureCollection.fromFeatures(features)
                )
            )

        style
            .addLayer(
                SymbolLayer(MARKER_SYMBOL_LAYER_ID, MARKER_GEO_SOURCE_ID)
                    .withProperties(
                        PropertyFactory.iconImage(MARKER_ICON_ID),
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.iconOffset(arrayOf(0f, -9f))
                    )
            )
    }

    /**
     * Draws labeled marker at specified [Coordinates].
     */
    fun drawLabeledMarker(markerDrawable: Drawable, coordinates: Coordinates, label: String) {
        mapBoxMap?.style?.let {

            val feature =
                Feature.fromGeometry(
                    Point.fromLngLat(
                        coordinates.longitude,
                        coordinates.latitude
                    )
                )

            feature.addStringProperty(
                "label",
                label
            )

            val labelFormat =
                Expression.format(
                    Expression.formatEntry(
                        Expression.get("label"),
                        Expression.FormatOption.formatTextColor(Color.BLACK),
                        Expression.FormatOption.formatFontScale(0.8)
                    )
                )

            it.addImage(
                BaseMapActivity.LABELED_MARKER_ICON_ID,
                markerDrawable
            )

            it.removeLayer(BaseMapActivity.LABELED_MARKER_SYMBOL_LAYER_ID)
            it.removeSource(BaseMapActivity.LABELED_MARKER_GEO_SOURCE_ID)

            it.addSource(
                GeoJsonSource(
                    BaseMapActivity.LABELED_MARKER_GEO_SOURCE_ID,
                    FeatureCollection.fromFeature(feature)
                )
            )

            it.addLayer(
                SymbolLayer(
                    BaseMapActivity.LABELED_MARKER_SYMBOL_LAYER_ID,
                    BaseMapActivity.LABELED_MARKER_GEO_SOURCE_ID
                )
                    .withProperties(
                        PropertyFactory.iconImage(BaseMapActivity.LABELED_MARKER_ICON_ID),
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.textField(labelFormat),
                        PropertyFactory.textOffset(arrayOf(0f, -1.75f))
                    )
            )

        }
    }

    /**
     * Draws custom view marker at specified [Coordinates].
     */
    fun drawCustomView(view: View, coordinates: Coordinates) {
        mapBoxMap?.style?.let {

            val feature =
                Feature.fromGeometry(
                    Point.fromLngLat(
                        coordinates.longitude,
                        coordinates.latitude
                    )
                )

            val bitmap = SymbolGenerator.generate(view)

            it.addImage(
                BaseMapActivity.CUSTOM_MARKER_ICON_ID,
                bitmap
            )

            it.removeLayer(BaseMapActivity.CUSTOM_MARKER_SYMBOL_LAYER_ID)
            it.removeSource(BaseMapActivity.CUSTOM_MARKER_GEO_SOURCE_ID)

            it.addSource(
                GeoJsonSource(
                    BaseMapActivity.CUSTOM_MARKER_GEO_SOURCE_ID,
                    FeatureCollection.fromFeature(feature)
                )
            )

            it.addLayer(
                SymbolLayer(
                    BaseMapActivity.CUSTOM_MARKER_SYMBOL_LAYER_ID,
                    BaseMapActivity.CUSTOM_MARKER_GEO_SOURCE_ID
                )
                    .withProperties(
                        PropertyFactory.iconImage(BaseMapActivity.CUSTOM_MARKER_ICON_ID),
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.iconOffset(arrayOf(0f, -9f))
                    )
            )

        }
    }

    /**
     * Draw marker at coordinates.
     *
     * NOTE: Make sure to call this after [onStyleLoaded] and after asking location permissions.
     */
    fun showUserLocationMarker() {
        val locationComponentActivationOptions =
            LocationComponentActivationOptions
                .builder(requireContext(), mapBoxMap!!.style!!)
                .locationComponentOptions(locationComponentOptions)
                .useDefaultLocationEngine(true)
                .build()

        mapBoxMap!!
            .locationComponent
            .apply {
                activateLocationComponent(locationComponentActivationOptions)
                isLocationComponentEnabled = true
                cameraMode = CameraMode.NONE
            }
    }

    override fun onResume() {
        super.onResume()
        getMapView()
            .onResume()
        initializeView()
        setupMapView()
    }

    override fun onStart() {
        super.onStart()
        getMapView()
            .onStart()
    }

    override fun onPause() {
        super.onPause()
        getMapView()
            .onPause()
    }

    override fun onStop() {
        super.onStop()
        getMapView()
            .onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        getMapView()
            .onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        getMapView()
            .onLowMemory()
    }
}