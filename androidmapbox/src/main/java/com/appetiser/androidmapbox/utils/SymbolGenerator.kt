package com.appetiser.androidmapbox.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.view.View

/**
 * Utility class to generate Bitmaps for Symbol.
 *
 * Reference: https://bit.ly/3f0eH1D
 *
 * Bitmaps can be added to the map with [com.mapbox.mapboxsdk.maps.MapboxMap.addImage]
 *
 */
object SymbolGenerator {
    /**
     * Generate a Bitmap from an Android SDK View.
     *
     * @param view the View to be drawn to a Bitmap
     * @return the generated bitmap
     */
    fun generate(view: View): Bitmap {
        val measureSpec: Int = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(measureSpec, measureSpec)
        val measuredWidth: Int = view.measuredWidth
        val measuredHeight: Int = view.measuredHeight
        view.layout(0, 0, measuredWidth, measuredHeight)
        val bitmap =
            Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
        bitmap.eraseColor(Color.TRANSPARENT)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
}