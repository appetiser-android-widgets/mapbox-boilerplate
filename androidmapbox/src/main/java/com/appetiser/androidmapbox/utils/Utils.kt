package com.appetiser.androidmapbox.utils

import android.content.Context
import android.location.Location
import com.appetiser.androidmapbox.models.Coordinates

fun Int.toPx(context: Context): Int = (this * context.resources.displayMetrics.density).toInt()

fun calculateRadius(
    topLeftLatitude: Double,
    boundingBoxCenterCoordinates: Coordinates
): Double {
    val distanceResults = FloatArray(2)

    // Calculate radius from top-center of bounding box to center.
    Location
        .distanceBetween(
            topLeftLatitude,
            boundingBoxCenterCoordinates.longitude,
            boundingBoxCenterCoordinates.latitude,
            boundingBoxCenterCoordinates.longitude,
            distanceResults
        )

    return (distanceResults[0] / 1000).toDouble() // convert meters to kilometers
}