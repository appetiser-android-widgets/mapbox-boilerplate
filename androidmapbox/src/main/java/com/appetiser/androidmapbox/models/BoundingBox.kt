package com.appetiser.androidmapbox.models

data class BoundingBox(
    val topLeft: Coordinates,
    val bottomRight: Coordinates
) {
    companion object {
        /**
         * Calculates the center coordinates from a bounding box.
         */
        fun getBoundingBoxCenterCoordinates(
            topLeftLatitude: Double,
            topLeftLongitude: Double,
            bottomRightLatitude: Double,
            bottomRightLongitude: Double
        ): Coordinates {
            val boundingBoxCenterLatitude = (topLeftLatitude + bottomRightLatitude) / 2
            val boundingBoxCenterLongitude = (topLeftLongitude + bottomRightLongitude) / 2

            return Coordinates(
                boundingBoxCenterLatitude,
                boundingBoxCenterLongitude
            )
        }
    }
}