package com.appetiser.androidmapbox.models

data class Coordinates(
    val latitude: Double,
    val longitude: Double
)